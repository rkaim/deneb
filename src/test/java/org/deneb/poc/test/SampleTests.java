package org.deneb.poc.test;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.deneb.poc.http.handler.HttpRequestHandler;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * Sample Test class
 * @author Rakesh Kaim
 */
@SuppressWarnings("deprecation")
public class SampleTests {

	/**
	 * Sample test data
	 */
	
	private static Map<String, String> inputs = new HashMap<String, String>();
	private HttpRequestHandler requestHandler;
	/**
	 * Initialize sample data before each test execution
	 */
	
	@BeforeClass
	public static void init() {
		try (InputStream input = new FileInputStream("src/test/resources/config.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            inputs.put("deneb.url.home", prop.getProperty("deneb.url.home"));
            inputs.put("deneb.url.facebook", prop.getProperty("deneb.url.facebook"));
            inputs.put("deneb.url.amazon", prop.getProperty("deneb.url.amazon"));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
	
	@Before
	public void setUp() {
		requestHandler = new HttpRequestHandler();
	}

	/**
	 * Destroy sample data after each test execution
	 */
	@After
	public void tearDown(){
		requestHandler = null;
	}
	
	/**
	 * Sample test method .
	 * 
	 */
	@Test
	public final void testAdd() {
		assertTrue(true);
	}
	
	@Test
	public final void testGetFacebook() {
		
		Document response = requestHandler.getDocument(inputs.get("deneb.url.facebook"));
		System.out.println(response.toString());
		assertTrue(true);
	}
}

