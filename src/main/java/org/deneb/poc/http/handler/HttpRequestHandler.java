package org.deneb.poc.http.handler;

import java.io.IOException;

import static org.jsoup.Jsoup.*;
import org.jsoup.nodes.Document;

public class HttpRequestHandler {

	public Document getDocument(String url) {
		Document document = null;
		try {
			document = connect(url).execute().parse();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return document;
	}
}
